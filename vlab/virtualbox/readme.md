
## Proxmox Voraussetzungen vLAB
* Virtualisierungslösung (Virtualbox / VMware Player oder Workstation)
* 4 GB Hauptspeicher
* 1 - 2 x Core CPU
* zirka 60 - 100 GB Speicherplatz
Achtung Netz/Homelaufwerk -> USB Stick -> Laptop

---
## Proxmox Installation (Manual)
* Download Proxmox ISO (Version 5.3)
  * https://www.proxmox.com/en/downloads/item/proxmox-ve-5-3-iso-installer

---
## Virtualbox VM erstellen

![sam-vlab-proxmox-virtualbox-vm](../_images/sam-vlab-proxmox-virtualbox-vm.png)

---
## Proxmox Installation

---
# SAM Tip Proxmox UI on localhost
* Virtualbox NAT Einstellungen - Portweiterleitung 8006
![sam-vlab-proxmox-nat](../../_images/sam-vlab-proxmox-nat.png)

---
## Virtualbox Networking
* Promiscios Mode for Bridging
* Einstellungen --> Netzwerk

![sam-vlab-proxmox-virtualbox-network](../../_images/sam-vlab-proxmox-virtualbox-network-1.png)

---
## Virtualbox Networking
* VM Network Adapter

![sam-vlab-proxmox-virtualbox-network](../../_images/sam-vlab-proxmox-virtualbox-network-2.png)

---

![sam-vlab-proxmox-virtualbox-network](../../_images/sam-vlab-proxmox-virtualbox-network-3.png)

---
## Virtualbox Nested Virtualistion
* Virtualbox 6.0
* Manueles aktivieren (AMD CPUs)
  ```
  vboxmanage modifyvm sam-vlab-proxmox-node-1 --nested-hw-virt on
  ```
https://docs.oracle.com/cd/E97728_01/F12469/html/nested-virt.html

---
## Überprüfen der Virtualisierungserweiterung
### Intel
```
cat /proc/cpuinfo | grep --color vmx
```
### AMD
```
cat /proc/cpuinfo | grep --color svm
```
