## Systemadministration Übung
### Virtualisierung und Container


---
## Proxmox
* Virtualisierung und Container
* Version 20200310

---
## Proxmox vLAB Übersicht
* Einführung Proxmox
* Voraussetzungen vLAB
* Installation von Proxmox
* Proxmox mit Container
* Proxmox mit Virtuellen Maschinen


---
## Einführung Proxmox
#### Debian basierende Linux Distribution für Virtualisierung mit Webbasierter Bedienungsoberfläche

---
## Virtualisierungen
* Container LXC
* Vollvirtualisierung KVM

---
## Speicheranbindungen
#### Lokal
* ext4
* LVM
* ZFS

---
## Speicheranbindungen
#### Netzwerk
* NFS
* ISCSI

---
## Speicheranbindungen
#### Objektbasiert
* CEPH / RDB

---
## Proxmox Voraussetzungen vLAB
* Virtualisierungslösung (Virtualbox / VMware Player oder Workstation)
* 4 GB Hauptspeicher
* 1 - 2 x Core CPU
* zirka 20 GB Speicherplatz

Achtung: Homelaufwerk/Netz -> USB Stick -> Laptop

---
## Proxmox Installation (Manual)
* Download Proxmox ISO (Version 6.1)
  * https://www.proxmox.com/de/downloads/item/proxmox-ve-6-1-iso-installer

---
## VM erstellen (VMware)
#### Proxmox Installation via ISO

![sam-vlab-proxmox-vmwareplayer-vm](_images/vmwareplayer-new-1.png)

---
## VM erstellen (VMware)
#### Guest OS Debian 9.0 x64

![sam-vlab-proxmox-vmwareplayer-vm](_images/vmwareplayer-new-2.png)

---
## VM erstellen (VMware)
#### Hardware verändern

![sam-vlab-proxmox-vmwareplayer-vm](_images/vmwareplayer-new-3.png)

---
## VM erstellen (VMWare)
#### Hauptspeicher 6 GB

![sam-vlab-proxmox-vmwareplayer-vm](_images/vmwareplayer-new-4.png)

---
## VM erstellen (VMware)
#### CPU 2 Kerne

![sam-vlab-proxmox-vmwareplayer-vm](_images/vmwareplayer-new-5.png)

---
## VM erstellen (VMware)
#### Network Adapter (NAT)

![sam-vlab-proxmox-vmwareplayer-vm](_images/vmwareplayer-new-6.png)

---
## Installation (Proxmox)
#### Hardware Virtualisierung

![sam-vlab-proxmox-install-1](_images/proxmox-install-1.png)

---
## Installation (Proxmox)
#### Systemdisk

![sam-vlab-proxmox-install-2](_images/proxmox-install-2.png)

---
## Installation Proxmox
#### Netzwerkeinstellungen

![sam-vlab-proxmox-install-2](_images/proxmox-install-3.png)

---
##  Architektur

![sam-vlab-proxmox-architektur](_images/sam-ue-proxmox-start.png)

---
## Proxmox Login

* https://localhost:8006
* user: root
* pwd: geheim

---  
## First Login

![sam-vlab-proxmox-installed](_images/sam-vlab-proxmox-installed.png)

---
## Architektur Proxmox

* Datacenter
  * Node

---
## Node

* Summary
* Shell
* Networking
* DNS

---
## Disks
  * LVM
  * ZFS

---
## Datastores
* Contenttypes

---
##  Überblick

![sam-vlab-proxmox-architektur](_images/sam-ue-proxmox-start-1.png)

---
## Netzwerke (Vmware)

* Bridge
* NAT
* Hostonly
* Custom

---
## Netzwerk Linux
#### Troubleshooting Promiscios Mode for Bridges

`sudo chmod a+rw /dev/vmnet2`

---
## Netzwerkadapter

* Netzwerkadapter Hinzufügen (VMware)
* NAT
* Reboot Proxmox

---
## Proxmox Netzwerk

* Add Bridge Interface
* Add Bridge Binding
* Reboot Proxmox

---
## Einführung Container

![Image Container Aufbau](_images/sam-vlab-proxmox-containeraufbau.png)

---
## Einführung Container

* Containerengine LXC

---
## Eigenschaften Compute

* Single Kernel Image
* Security über
  * Abstraktion über Namespaces and Cgroups
  * Appamor and SELinux

---
## Eigenschaften Storage

* Abstraktion Folder
* Abstraktion DiskImage

---
## Eigenschaften Netzwerk

* Kernel Namespaces
* Abstraktion Virtuelles Netzwerk

---
## First Steps with Container

Wie Startet ein Container ?

---
## First Steps with Container
#### Download Template (Empfehlung Ubuntu 18.04)

![Container Template Download](_images/sam-proxmox-lxc-template.png)

---
## Container Erstellen

![](_images/sam-vlab-proxmox-create-lxc-1.png)

---
## Container Template

![](_images/sam-vlab-proxmox-create-lxc-2.png)

---
## Container Storage

![](_images/sam-vlab-proxmox-create-lxc-3.png)

---
## Container Network
![](_images/proxmox-container-net.png)

---
## Summary

![](_images/sam-vlab-proxmox-lxc-8.png)

---
## Container Starten

![](_images/sam-vlab-proxmox-lxc-8.png)


---
## Aufgabe

#### Überprüfen der Container Sicherheit bezüglich Cgroups und Namespaces

---
## Erstellen eines Containers
#### sam-container-1

---
## Installation eines Webservers
#### (Gastsystem/sam-container-1)

`apt-get install apache2`

---
## Testen des Webservers
#### (Gastsystem/sam-container-1)

---
## Sichten der Umgebung
#### (Gastsystem/sam-container-1)

* df -h
* ps -awx
* tree ~

---
## Erstellen eines Containers
#### sam-container-2

---
## Installation eines Webservers (ngnix)
#### (Gastsystem/sam-container-2)

---
## Testen des Webservers
#### (Gastsystem/sam-container-2)

---
## Sichten der Umgebung
#### (Gastsystem/sam-container-2)

* df -h
* ps -awx
* tree ~

---
## Hostsystem
#### Betrachtung der Prozesse / Security zwischen den Containern am Hostsystem

  * df -h
  * ps -awx
  * tree ~

---
## Aufgabe

#### Usecase Website Defacement

---
## Erstellen einer Webseite
#### (Gastsystem/sam-container-1)

`echo "<html><p>Container SAM-CT-1 Produktion</p></html>" > /var/www/html/index.html`

---
## Create Snapshot
#### (Gastsystem/sam-container-1)

* einmalig
* kontinuierlich

---
## Simulation eines Hacks
### Website Deface

`echo "<html><p>Container SAM-CT-1 Hacked</p></html>" > /var/www/html/index.html`

---
## Revert Snapshot

* Überprüfen des Inhalts
* less /var/www/html/index.html

---
## Container Backup

* Container Backup Erstellen

---
## Container Recover

* Transfer und Import des Containers beim Nachbarn
* Tipp Transfer über Aconet Filesender (https://filesender.aco.net)
* Überprüfen der Funktionen des Containers

---
## Summary Proxmox mit Container
* Nested Virtualisation
* Netzwerkanbindung eines Containers ( NAT / Bridged / Routed)
* Disk / IO System des Containers (Host / Gast)

---

# Lessions Learned ?
* War eine Nested Virtalisation nötig ?

---
## Einführung Virtuelle Maschine
![](_images/sam-vlab-proxmox-vm.png)

---
## Einführung VM
* Eigenen Prozess und Hauptspeicherbereich
  * Abstraktion über VMM
* Eigenen Storage / Speicherbereich  
  * Abstraktion über Disk Imageformat oder Schnittstelle
* Netzwerkschnittstelle

---
## Überprüfen der HW Virtualisierungserweiterung
### Intel

`cat /proc/cpuinfo | grep --color vmx`

### AMD

`cat /proc/cpuinfo | grep --color svm`

---
## Nested Virtualisation (Vmware)
![sam-vlab-proxmox-vmwareplayer-vm](_images/sam-vlab-proxmox-vmware-nested.png)

---
## First Steps in Proxmox with VM
* VM Erstellen
* Installation Betriebsystem (Linux Ubuntu 18.04 LTS)

---
## Clone / Template einer VM erstellen
* Erstellen einer VM (vom Template)
  * Installation einer Webanwendung
    * apt-get install apache2

---    
* Erstellen einer VM (vom Template)
  * Installation einer Anwendung
    * apt-get install nginx

---
## Gastsystem
* Sichten der Umgebung innerhalb der VMs
  * df -h
  * ps -awx
  * tree ~

---
## Hostsystem
* Betrachtung der Prozesse / Security am Hostsystem
    * df -h
    * ps -awx
    * tree ~

---
## VM Snapshot Erstellen
* VM Login
* Create Snapshot
* Change Container Content
  * echo "<html><p>Container SAM-VM-1 Hacked</p></html>" > /var/www/html/index.html
* Revert Snapshot
  * ls -alh


---
## Aufgaben Poxmox mit Virtuellen Maschinen
* VM Backup Erstellen
* Transfer und Import der VM beim Nachbarn

---
## Summary Proxmox mit Container
* Nested Virtualisation
* Netzwerkanbindung eines Containers ( NAT / Bridged / Routed)
* Disk / IO System des Containers (Host / Gast)

---
## Summary VM
* Nested Virtualisation
* Netzwerkanbindung eines Containers ( Routed / Bridged)
* Disk / IO System des Containers (Host / Gast)

---
## SAM Übung Übersicht Summary
![sam-vlab-proxmox-nat](_images/sam-uebersicht-uebung.jpg)


---
## Proxmox Tipp
* Problem Nested Virtualisation (Matroschka)
* Image Virtualbox Hardware Virtualisierung

---
## Proxmox Cheat Sheet
* Loginname ist root
* KVM Nested Virtualisation
  * cat /sys/module/kvm_intel/parameters/nested
* Check Virtual Extensions in the VM
  * egrep -c "(svm|vmx)" /proc/cpuinfo
* Check Network Bridge
  * brctl show

---
# Links
* Porxmox Webseite
https://www.proxmox.com/de/
* Download Proxmox ISO (Version 6.1)
https://www.proxmox.com/de/downloads/item/proxmox-ve-6-1-iso-installer
* VMware Netzwerke
https://docs.vmware.com/en/VMware-Workstation-Player-for-Windows/15.0/com.vmware.player.win.using.doc/GUID-8FDE7881-C31F-487F-BEF3-B2107A21D0CE.html
* Linux Commandline Hints
http://teknixx.com/40-useful-linux-commands/
* Linux Namespaces and Cgroups
https://en.wikipedia.org/wiki/Cgroups#NAMESPACE-ISOLATION
