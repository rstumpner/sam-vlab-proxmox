# SAM vLab with for Proxmox
This is a Gitlab Repository to do an easy Virtual Lab Environment for Proxmox ( https://www.proxmox.com ) Open Source Software.

To Follow the Instrutions of this Virtual Lab:

Option 1 (Online):
  * Browse https://gitpitch.com/rstumpner/sam-vlab-proxmox/master?grs=gitlab&t=simple

Option 2 (Local):
  * git clone https://gitlab.com/rstumpner/sam-vlab-virt-proxmox
  * gem install showoff
  * showoff serve
  * Browse http://localhost:9090

Virtual Lab Environment Setup:

Requirements:
  * 3 GB Memory (minimal)
  * 2 x CPU Cores
  * Virtualbox

vLAB Setup:
  * proxmox-1 (Proxmox ISO Installation Node 1 )
  * proxmox-1 (Proxmox ISO Installation Node 2 )
